*VM for Fluffy operating system*

**The Fluffyest virtual machine in the world!!! ^.^**

* VM project is available in "docs" folder.

* VM program files ("*.fluffy") are available in "fluffies" folder.

To compile and run Fluffy VM on Linux simply type "make" and "make run" in terminal.