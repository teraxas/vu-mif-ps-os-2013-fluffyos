/**
 * 
 */
package fluffy.machine;

import java.io.IOException;

import fluffy.machine.registers.FRegister2B;
import fluffy.machine.registers.FRegister4B;
import fluffy.machine.registers.FRegisterInterrupt;
import fluffy.machine.registers.FRegisterLogic;
import fluffy.machine.registers.FRegisterLogicByte;
import fluffy.machine.virtual.VMachine;
import fluffy.machine.virtual.VMemory;


/**
 * @author karolis
 */
public class FMachine {
	
	/**
	 * THE ONLY VM
	 * NOT FOR MULTIPROGRAM USE!
	 */
	private VMachine theVM;
	
	/**
	 * Memory
	 */
	private FMemory memory;
	
	/**
	 * Data register
	 */
	private FRegister4B regR1, regR2;
	
	/**
	 * Code index register
	 */
	private FRegister2B regIC;
	
	/**
	 * 1 byte logic register
	 */
	private FRegisterLogicByte regC;
	
	/**
	 * Page Table register
	 */
	private FRegister4B regPLR;
	
	/**
	 * Stack pointer register
	 */
	private FRegister4B regSP;
	
	/**
	 * CPU mode register
	 */
	private FRegisterLogic regMODE;
	
	/**
	 * Program interrupts
	 */
	private FRegisterInterrupt regPI; 
	
	/**
	 * System interrupts
	 */
	private FRegisterInterrupt regSI;
	
	/**
	 * IOI register
	 * 1 - input
	 * 2 - output
	 */
	private FRegisterInterrupt regIOI;
	
	/**
	 * Channel registers
	 * K1 - input
	 * K2 - output
	 * IS CHANNEL USED? 
	 * True - used, False - free;
	 */
	private FRegisterLogic regK1, regK2, regK3;
	
	/**
	 * Timer register 
	 */
	private FRegisterInterrupt regTIME;

	
	/**
	 * Output channel
	 */
	private FChannelProcessor channelProcessor;
	
	/**
	 * Default constructor
	 */
	public FMachine() {
		this.memory = new FMemory();
		this.initDefaultRegisters();
		channelProcessor = new FChannelProcessor(regIOI, regK2, regK1);
	}
	
	
	/**
	 * Inits registers with default values
	 */
	private void initDefaultRegisters() {
		//Registers also used by VM
		this.regC = new FRegisterLogicByte();
		this.regIC = new FRegister2B();
		this.regR1 = new FRegister4B();
		this.regR2 = new FRegister4B();
		this.regSP = new FRegister4B();
		
		this.regPLR = new FRegister4B();
		
		this.regK1 = new FRegisterLogic();
		this.regK2 = new FRegisterLogic();
		this.regK3 = new FRegisterLogic();
		this.regMODE = new FRegisterLogic();
		this.regPI = new FRegisterInterrupt();
		this.regSI = new FRegisterInterrupt();
		this.regIOI = new FRegisterInterrupt();
		this.regTIME = new FRegisterInterrupt();
		
		this.regPI.setValue(0);
		this.regSI.setValue(0);
		this.regIOI.setValue(0);
	}
	
	/**
	 * Creates the only VM.
	 * NOT FOR MULTIPROGRAM USE!
	 * @param filepath filepath to *.fluffy file
	 * @throws IOException
	 */
	public void createTheVM(String filepath) throws IOException {
		this.theVM = newVirtualMachine();
		this.theVM.loadProgramFromFile(filepath);
		this.setFMRegisters(this.theVM);
	}
	
	/**
	 * Create new virtual machine
	 * @return new virtual machine
	 */
	private VMachine newVirtualMachine(){
		VMachine machine = new VMachine(
				this, newVirtualMemory());
		return machine;
	}
	
	/**
	 * Allocate memory
	 * Also sets PLR
	 * @return allocated virtual memory, null if failed
	 */
	private VMemory newVirtualMemory(){
		VMemory mem = null;
		try {
			mem = new VMemory(
					this.memory, this.regPLR.getValWord());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return mem;
	}
	
	/**
	 * main loop for single job (the VM)
	 * NOT FOR MULTIPROGRAM USE!
	 */
	public void mainLoopSingleJob() {
		Thread mainLoop = new Thread(new Runnable() {
			@Override
			public void run() {
				boolean indicator;
				
				do{
					indicator = stepCheck(theVM);
				} while (indicator);
				
				theVM.destroy();
				theVM = null;
			}
		});
		mainLoop.run();
	}
	
	/**
	 * Check if IOI is ok.
	 * If no IOI - step()
	 * If IOI is done - step()
	 * Else - skip
	 */
	public boolean stepCheck(VMachine vm) {
		boolean indicator = true;
		
		if (vm.getWaitingIOI() == 0){
			indicator = step(theVM);
		} else {
			if (vm.getWaitingIOI() == this.regIOI.getValue()){
				if(this.regIOI.getValue() == 1){ //Input
					this.channelProcessor.stopInput(
							vm.getMemory(), this.regR2.getValInt(),
							this.regR1.getValInt());
					//Now we can free the channel
					this.regK1.setValue(false);
				} else if (this.regIOI.getValue() == 2){//Output
					this.regK2.setValue(false);
				}
				indicator = step(theVM);
				vm.setWaitingIOI(0);
			}
		}
		return indicator;
	}
	
	/**
	 * Step for the Fluffy machine.
	 * No IOI check.
	 */
	public boolean step(VMachine vm) {
		boolean res = true;
		
		try{
			vm.step();
		} catch (IllegalArgumentException e){
			//Illegal command or argument
			this.regPI.setValue(1);
			System.out.println(vm.getName() + ": " + e);
		}
		
		if (this.testForInterrupts() != 0){//TODO set actions on all interrupts
			switch(getSI().getValue()){
				//TODO TEST K[x] if locked (only with multiprogram)
				case 0: 
					break;
				case 1://Input
					System.out.println("Input is needed");
					this.regSI.setValue(0);
					
					//if (!regK1.isValue()){//NOT used
						this.channelProcessor.startInput();
						vm.setWaitingIOI(1);
					//} else { //used
					//	int ic = regIC.getValInt();
					//	ic--;
					//	regIC.setValInt(ic);
					//}
					
					break;
				case 2://Output
					System.out.println("Output is needed");
					this.regSI.setValue(0);
					
					//if (!regK2.isValue()){//NOT used
						this.channelProcessor.startOutput(
								vm.getMemory(), this.regR2.getValInt(),
								this.regR1.getValInt());
						vm.setWaitingIOI(2);
					//} else { //used
					//	int ic = regIC.getValInt();
					//	ic--;
					//	regIC.setValInt(ic);
					//}
					
					break;
				case 3://HALT
					System.out.println("HALT");
					this.regSI.setValue(0);
					res = false;
					break;
			}
			switch(getPI().getValue()){
				case 0: break;
				case 1: 
					this.channelProcessor.simpleOutput("ILLEGAL command or argument");
					res = false; //HALT the machine
					this.regPI.setValue(0);
					break;
				case 2: 
					System.out.println("Result is negative");
					this.regPI.setValue(0);
					break;
				case 3: 
					System.out.println("Black hole has opened");
					res = false;
					this.regPI.setValue(0);
					break;
				case 4: 
					System.out.println("Overflow");
					res = false;
					this.regPI.setValue(0);
					break;
			}
		}
		return res;
	}
	
	/**
	 * TEST registers for interrupts
	 */
	private int testForInterrupts() {
		int res = 0;
		
		//Check for interrupts
		if (this.regPI.getValue() != 0 ||
				this.regSI.getValue() != 0){
			res = 1;
		}
		return res;
	}
	
	/**
	 * sets registers of FM by the ONLY VM
	 * NOT FOR MULTIPROGRAM USE!
	 */
	private void setFMRegisters(VMachine vm) {
		this.regC = vm.getC();
		this.regIC = vm.getIC();
		this.regR1 = vm.getR1();
		this.regR2 = vm.getR2();
		this.regSP = vm.getSP();
	}
	
	
	/**
	 * @return the r1
	 */
	public FRegister4B getR1() {
		return regR1;
	}

	/**
	 * @return the r2
	 */
	public FRegister4B getR2() {
		return regR2;
	}

	/**
	 * @return the iC
	 */
	public FRegister2B getIC() {
		return regIC;
	}

	/**
	 * @return the c
	 */
	public FRegisterLogicByte getC() {
		return regC;
	}

	/**
	 * @return the pLR
	 */
	public FRegister4B getPLR() {
		return regPLR;
	}

	/**
	 * @return the sP
	 */
	public FRegister4B getSP() {
		return regSP;
	}

	/**
	 * @return the mODE
	 */
	public FRegisterLogic getMODE() {
		return regMODE;
	}

	/**
	 * @return the pI
	 */
	public FRegisterInterrupt getPI() {
		return regPI;
	}

	/**
	 * @return the sI
	 */
	public FRegisterInterrupt getSI() {
		return regSI;
	}

	/**
	 * @return the iOI
	 */
	public FRegisterInterrupt getIOI() {
		return regIOI;
	}

	/**
	 * @return the k1
	 */
	public FRegisterLogic getK1() {
		return regK1;
	}

	/**
	 * @return the k2
	 */
	public FRegisterLogic getK2() {
		return regK2;
	}

	/**
	 * @return the k3
	 */
	public FRegisterLogic getK3() {
		return regK3;
	}

	/**
	 * @return the tIME
	 */
	public FRegisterInterrupt getTIME() {
		return regTIME;
	}
	
	public FMemory getMemory(){
		return this.memory;
	}

}
