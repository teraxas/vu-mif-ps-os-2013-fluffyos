package fluffy.machine;

import fluffy.gui.FGDeviceIOI;
import fluffy.machine.registers.FRegisterInterrupt;
import fluffy.machine.registers.FRegisterLogic;
import fluffy.machine.virtual.VMemory;

public class FChannelProcessor{
	
	/**
	 * IO device - GUI window with Input and Output
	 */
	private FGDeviceIOI ioDevice;
	
	/**
	 * Registers of FM
	 */
	private FRegisterInterrupt regIOI;
	private FRegisterLogic regK1;
	private FRegisterLogic regK2;
	
	/**
	 * buffers
	 */
	private String bufferOutput;
	private String bufferInput; 

	public FChannelProcessor(FRegisterInterrupt regIOI, 
			FRegisterLogic regK2,
			FRegisterLogic regK1){
		this.regIOI = regIOI;
		this.regK2 = regK2;
		this.regK1 = regK1;
	}
	
	/**
	 * Starts Output
	 * @param vMem
	 * @param vStartAddress
	 * @param type 1 - number, 2 - string
	 */
	public void startOutput(VMemory vMem, int vStartAddress,
			int type){
		makeDevice();
		int address = vStartAddress;
		String word;
		this.bufferOutput = "";
		
		switch (type){
		case 1://OUTPUT Number
			this.bufferOutput = String.valueOf(
					vMem.getWordAtAddress(address).getValInt());
			break;
		case 2://OUTPUT String
			do {
				word = getWord(vMem, address);
				this.bufferOutput = this.bufferOutput + word;
				address++;
			} while (!isLast(word));
			this.bufferOutput = this.bufferOutput.substring(
					0, bufferOutput.length()-1);
			break;
		case 3://OUTPUT Hex number
			this.bufferOutput = getWord(vMem, address);
		default:
			throw new IllegalArgumentException(
					"Wrong output type: " + type);
		}
		
		
		this.ioDevice.makeOutput(bufferOutput);
		
		System.out.println("OUTPUT: type(" + type + "): " + bufferOutput);
		regIOI.setValue(2);
		regK2.setValue(false);
	}
	
	/**
	 * Start input
	 */
	public void startInput() {
		makeDevice();
		regIOI.setValue(0);
		ioDevice.makeInput();
		System.out.println("Waiting for input...");
	}
	
	/**
	 * Stop input
	 * @param vMem
	 * @param vStartAddress
	 * @param type 1 - number, 2 - string
	 */
	public void stopInput(VMemory vMem, int vStartAddress,
			int type) {
		this.bufferInput = ioDevice.getBuffer();
		 
		switch (type){
		case 2: //String
			//SPLIT data
			String subLine = bufferInput + "#";
			String chars4;
			int writeAddress = vStartAddress;
			while (subLine.length() > 4) {
				chars4 = getFirst4Bytes(subLine);
				subLine = getValue(subLine, 4);
				setMemVal(chars4, writeAddress, vMem);
				writeAddress++;
			}
			setMemVal(subLine, writeAddress, vMem);
			break;
		case 1: //Integer
			/*vMem.getWordAtAddress(vStartAddress).setValInt(
					Integer.parseInt(bufferInput));*/
			setMemValInt(bufferInput, vStartAddress, vMem);
			break;
		default:
			throw new IllegalArgumentException("TYPE: " + type);
		}
	}
	
	/**
	 * Sets memory value
	 * @param line line to add
	 * @param address where to add
	 */
	private void setMemVal(String line, int address, VMemory vMem){
		vMem.getWordAtAddress(address).setVal(line);
		System.out.println("INPUT: " + address + " : " + line);
	}
	
	/**
	 * Sets memory value
	 * @param line line to add
	 * @param address where to add
	 */
	private void setMemValInt(String line, int address, VMemory vMem){
		try{
			int numb = (Integer.parseInt(line));
			String hex = Integer.toHexString(numb);
			this.setMemVal(hex, address, vMem);
		} catch (NumberFormatException e){
			System.out.println("NFE :(");
		}
	}
	
	/**
	 * Get first 4 chars of string
	 * @param line
	 * @return string of first 4 chars
	 */
	private String getFirst4Bytes(String line){
		String newString = new String("");
		for (int i = 0; i != 4; i++){
			try {
				newString = newString + line.charAt(i);
			} catch (IndexOutOfBoundsException e){
				break;
			}
		}
		return newString;
	}
	
	/**
	 * get value from line
	 * Example: ".NAM hahaha"
	 * 	getValue(".NAM hahaha", 5) -> "hahaha"
	 * @param line
	 * @param valuePoint point where value begins
	 * @return value of line
	 */
	private String getValue(String line, int valuePoint) {
		String value = line.substring(valuePoint);
		return value;
	}
	
	
	
	/**
	 * Get String value of word at address
	 * @param vMem Virtual memory
	 * @param vAddress Address at virtual memory
	 * @return String value of given word
	 */
	private String getWord(VMemory vMem, int vAddress) {
		String str = vMem.getWordAtAddress(vAddress).getVal();
		return str;
	}
	
	/**
	 * Checks if the given words last symbol is "#"
	 * @param word
	 * @return true if the last symbol is "#"
	 */
	private boolean isLast(String word) {
		for(int i = 0; i < 4; i++){
			if (word.charAt(i) == '#')
				return true;
		}
		return false;
			
	}

	/**
	 * @return the regK1
	 */
	public FRegisterLogic getRegK1() {
		return regK1;
	}


	/**
	 * @param regK1 the regK1 to set
	 */
	public void setRegK1(FRegisterLogic regK1) {
		this.regK1 = regK1;
	}


	/**
	 * @return the bufferInput
	 */
	public String getBufferInput() {
		return bufferInput;
	}

	/**
	 * Simple output for machine error reports
	 * @param error Error string to print
	 */
	public void simpleOutput(String error) {
		makeDevice();
		ioDevice.makeOutput(error);
	}
	
	public void makeDevice() {
		if (this.ioDevice == null){
			this.ioDevice = new FGDeviceIOI(regIOI);
			this.ioDevice.setEnabled(true);
		}
	}


}
