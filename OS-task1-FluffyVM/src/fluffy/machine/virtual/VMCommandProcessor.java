/**
 * 
 */
package fluffy.machine.virtual;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import fluffy.machine.memory.FWord;
import fluffy.machine.registers.FRegister4B;
import fluffy.machine.registers.FRegisterLogicByte;

/**
 * Utility class for command processing and loading to memory
 * Makes things go fluffy
 * @author karolis
 */
public class VMCommandProcessor {

	/**
	 * The Virtual machine
	 */
	VMachine machina;
	
	/**
	 * Constructor
	 * @param machina Virtual machine
	 */
	public VMCommandProcessor(VMachine machina) {
		this.machina = machina;
	}
	
	/**
	 * Recognizes a command for virtual machine
	 * @param machina virtual machine
	 * @param command 4-byte command
	 * @return Was command recognition successful?
	 * @throws IllegalArgumentException
	 */
	public void processCommand(String command){
		char[] chars = command.toCharArray();
		boolean error = false;
		
		System.out.println(machina.getName() + ": " + command
				+ " at " + (machina.getIC().getValInt() - 1));
		
		if (command.equals("HALT")){
			cmHALT();
			return;
		}
		
		switch(chars[0]){
		//---L***
		case 'L': //LOAD
			switch(chars[1]){
			case '1':
				cmLzxy(command);
				break;
			case '2':
				cmLzxy(command);
				break;
			default:
				error = true;
				break;
			}
			break;
			
		//S***
		case 'S': //SAVE
			switch(chars[1]){
			case '1':
				cmSzxy(command);
				break;
			case '2':
				cmSzxy(command);
				break;
			case 'E'://Register SETTERS
				switch(chars[2]){
				case 'I':
					if (command == "SEIC"){ //SEIC
						cmSEIC();
					} else {
						error = true;
					}
					break;
				case 'S':
					if (command == "SESP"){ //SESP
						cmSESP();
					} else {
						error = true;
					}
					break;
				case 'C': //SECx
					cmSECx(command);
				}
				break;
			default:
				error = true;
			}
			break;
		
		//C***
		case 'C':
			switch (chars[1]){
			case '1': //Cmp1 - C1xy
				cmCzxy(command);
				break;
			case '2': //Cmp2 - C2xy
				cmCzxy(command);
				break;
			case 'P': //CPYx
				if (command.charAt(2) == 'Y'){
					cmCPYx(command);
				} else {
					error = true;
				}
				break;
			}
			break;
		
		//A***
		case 'A':
			switch (chars[1]) {
			case '1': //Add
				cmABMzxy(command);
				break;
			case '2': //Add
				cmABMzxy(command);
				break;

			default:
				error = true;
			}
			break;
			
		//B***
		case 'B':
			switch(chars[1]){
			case '1':
				cmABMzxy(command);
				break;
			case '2':
				cmABMzxy(command);
				break;
			default:
				error = true;
			}
			break;
			
		//M***
		case 'M':
			switch(chars[1]){
			case '1': //Multiply 1
				cmABMzxy(command);
				break;
			case '2': //Multiply 2
				cmABMzxy(command);
				break;
			default:
				error = true;
			}
			break;
			
		//D***
		case 'D':
			switch(chars[1]){
			case 'I': //DIxy
				cmDIxy(command);
				break;
			case 'E': //DEC
				if (command == "DEC1"){
					cmDecInc(command);
				} else if (command == "DEC2"){
					cmDecInc(command);
				}
				break;
			default:
				error = true;
			}
			break;
			
		//I***
		case 'I':
			switch(chars[1]){
			case 'N': //INC
				if (command.equals("INC1")){
					cmDecInc(command);
				} else if (command.equals("INC2")){
					cmDecInc(command);
				} else { //INxy - Input
					cmINxy(command);
				}
				break;
			default:
				error = true;
			}
			break;
			
		//J***
		case 'J':
			switch(chars[1]){
			//Fluffy JUMPS!!! 
			case 'P': //JP
				cmJUMPxy(command);
				break;
			case 'E': //JE
				cmJUMPxy(command);
				break;
			case 'L': //JL
				cmJUMPxy(command);
				break;
			case 'G': //JG
				cmJUMPxy(command);
				break;
			}
			break;
			
		//O*** (OOOOoooo... Fluffy)
		case 'O':
			if (chars[1] == 'P'){ //OPxy - Output
				cmOPxy(command);
			} else {
				error = true;
			}
			break;
			
		//G***
		case 'G':
			switch (chars[1]){
			case 'E': //Register GETTERS
				switch (chars[2]){
				case 'I':
					if (command == "GEIC"){ //GEIC
						this.cmGEIC();
					} else {
						error = true;
					}
					break;
				case 'C'://GECx
					cmGECx(command);
					break;
				case 'S':
					if (command == "GESP"){ //GESP
						cmGESP();
					} else {
						error = true;
					}
					break;
				default:
					error = true;
					break;
				}
			default:
				error = true;
				break;
			}
			break;
			
			default:
				error = true;
				break;
			
		}
		
		if (error) throw new IllegalArgumentException(command);
	}
	
	/**
	 * Loads a program from file to VM memory
	 * @param file
	 * @throws IOException 
	 */
	public void loadMemory(File source) throws IOException{
		//In Java working with files is not fluffy at all :(
		FileInputStream fis = new FileInputStream(source);
		InputStreamReader isr = new InputStreamReader(fis);
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(isr);
		
		int dsSize = 0;//Data segment size
		
		//Now let's read fluffy stuff :)
		int writeAddress = 0;
		String line;
		String subLine = null;
		String chars4 = null;
		char firstChar;
		
		while ((line = reader.readLine()) != null){
			firstChar = line.charAt(0);
			if (line.length() > 4){ //Data or values
				if (firstChar == '.'){ //Headers
					if (this.getFirst4Bytes(line).equals(".NAM")){
						//Name
						machina.setName(getValue(line, 5));
					} else if (this.getFirst4Bytes(line).equals(".DAT")) {
						//Data segment size
						try{
							dsSize = Integer.parseInt(this.getValue(line, 5));
						} catch (NumberFormatException e){
							e.printStackTrace();
							throw(e);
						}
					}
				} else if (firstChar == '/'){
					//Nothing - it's a comment
				}else if (firstChar == '*' && line.length() == 5){
					//DECIMAL NUMBER
					setMemValInt(this.getValue(line, 1), writeAddress);
					writeAddress++;
				} else { //Data
					//SPLIT data
					subLine = line;
					do {
						chars4 = getFirst4Bytes(subLine);
						subLine = getValue(subLine, 4);
						setMemVal(chars4, writeAddress);
						writeAddress++;
					} while (subLine.length() > 4);
					setMemVal(subLine, writeAddress);
					writeAddress++;
				}	
			} else { //Commands, data, addressing (<4)
				if (firstChar == '$'){ //Addressing or header
					if (line.equals("$FLJ")){
						//Header
					} else if (line.equals("$END")){
						System.out.println("Be nice - be FLUFFY!!!");
						//end of file
						break;
					} else if (line.equals("$FLC")) {//GOTO code
						writeAddress = 0;
					} else if (line.equals("$DAT")) {//GOTO data segment
						writeAddress = 
							10 * (this.machina.getMemory().DEFAULT_MEMSIZE - dsSize);
					} else{
						writeAddress = Integer.parseInt(getValue(line, 1));
					}
				} else if (firstChar == '/'){
					//Nothing - it's a comment
				} else if (firstChar == '*'){
					//DECIMAL NUMBER
					setMemValInt(getValue(line, 1), writeAddress);
					writeAddress++;
				} else { //Code or data (hex number?)
					setMemVal(line, writeAddress);
					writeAddress++;
				}
			}
		}
	}
	
	/**
	 * get value from line
	 * Example: ".NAM hahaha"
	 * 	getValue(".NAM hahaha", 5) -> "hahaha"
	 * @param line
	 * @param valuePoint point where value begins
	 * @return value of line
	 */
	private String getValue(String line, int valuePoint) {
		String value = line.substring(valuePoint);
		return value;
	}
	
	/**
	 * Get first 4 chars of string
	 * @param line
	 * @return string of first 4 chars
	 */
	private String getFirst4Bytes(String line){
		String newString = new String("");
		for (int i = 0; i != 4; i++){
			try {
				newString = newString + line.charAt(i);
			} catch (IndexOutOfBoundsException e){
				break;
			}
		}
		return newString;
	}
	
	/**
	 * Sets memory value
	 * @param line line to add
	 * @param address where to add
	 */
	private void setMemVal(String line, int address){
		this.machina.getMemory().getWordAtAddress(address).setVal(line);
	}
	
	/**
	 * Sets memory value
	 * @param line line to add
	 * @param address where to add
	 */
	private void setMemValInt(String line, int address){
		try{
			int numb = (Integer.parseInt(line));
			String hex = Integer.toHexString(numb);
			this.setMemVal(hex, address);
		} catch (NumberFormatException e){
			System.out.println("NFE :(");
		}
	}
	
//----C O M M A N D S----
	
	/**
	 * Lzxy
	 * Load to R[z] from [x*10+y] OR  [R[y]]
	 * @param cmd
	 * @throws IllegalArgumentException
	 */
	private void cmLzxy(String cmd) throws IllegalArgumentException{
		FRegister4B destination = null;
		int source;
		
		if (cmd.charAt(1) == '1'){
			destination = this.machina.getR1();
		} else if (cmd.charAt(1) == '2'){
			destination = this.machina.getR2();
		} else {
			throw new IllegalArgumentException(cmd);
		}
		
		if (cmd.charAt(2) == 'R'){
			switch (cmd.charAt(3)){
			case '1':
				source = this.machina.getR1().getValInt();
				break;
			case '2':
				source = this.machina.getR2().getValInt();
				break;
			default:
				throw new IllegalArgumentException(cmd);
			}
		} else {
			source = Integer.parseInt(getValue(cmd, 2));
		}
		
		destination.setValStr(machina.getMemory().
				getWordAtAddress(source).getVal());
	}
	
	/**
	 * Szxy
	 * Save from R[z] to [x*10+y] OR  [R[y]]
	 * @param cmd
	 * @throws IllegalArgumentException
	 */
	private void cmSzxy(String cmd) throws IllegalArgumentException{
		FRegister4B src = null;
		int dest;
		
		if (cmd.charAt(1) == '1'){
			src = this.machina.getR1();
		} else if (cmd.charAt(1) == '2'){
			src = this.machina.getR2();
		} else {
			throw new IllegalArgumentException(cmd);
		}
		
		if (cmd.charAt(2) == 'R'){
			switch (cmd.charAt(3)){
			case '1':
				dest = this.machina.getR1().getValInt();
				break;
			case '2':
				dest = this.machina.getR2().getValInt();
				break;
			default:
				throw new IllegalArgumentException(cmd);
			}
		} else {
			dest = Integer.parseInt(getValue(cmd, 2));
		}
		
		this.machina.getMemory().getWordAtAddress(dest).setVal(src.getValStr());
	}
	
	/**
	 * CPYx
	 * Copy value to R[x] from R[y];
	 * @param command
	 * @throws IllegalArgumentException
	 */
	private void cmCPYx(String command) throws IllegalArgumentException{
		if (command.charAt(3) == 1){ //CPY1
			this.machina.getR1().setValStr(
					this.machina.getR2().getValStr());
		} else if (command.charAt(3) == 2) { //CPY2
			this.machina.getR2().setValStr(
					this.machina.getR1().getValStr());
		} else {
			throw new IllegalArgumentException(command);
		}
	}
	
	/**
	 * Azxy/Bzxy/Mzxy - Addition AND Subtraction AND Multiplication
	 * R[z] + [x*10 + y]
	 * @param command
	 * @throws IllegalArgumentException
	 */
	private void cmABMzxy(String command) throws IllegalArgumentException{
		FRegister4B destination = null;
		int numb;
		
		if (command.charAt(1) == '1'){
			destination = this.machina.getR1();
		} else if (command.charAt(1) == '2'){
			destination = this.machina.getR2();
		} else {
			throw new IllegalArgumentException(command);
		}
		
		if (command.charAt(2) == 'R'){
			switch (command.charAt(3)){
			case '1':
				numb = this.machina.getR1().getValInt();
				break;
			case '2':
				numb = this.machina.getR2().getValInt();
				break;
			default:
				throw new IllegalArgumentException(command);
			}
		} else {
			numb = this.machina.getMemory().
					getWordAtAddress(
							Integer.parseInt(getValue(command, 2))).
							getValInt();
		}
		
		int res = 0;
		if (command.charAt(0) == 'A'){ //Addition
			res = destination.getValInt() + numb;
		} else if (command.charAt(0) == 'B'){ //Subtraction
			res = destination.getValInt() - numb;
		} else if (command.charAt(0) == 'M'){
			res = destination.getValInt() * numb;
		} else {
			throw new IllegalArgumentException(command);
		}
		
		destination.setValInt(res);
		
		//If result is longer than FWord - Overflow
		if(Integer.toString(res, 16).length() > FWord.DEFAULT_WORD_SIZE){
			machina.getC().setFlagOverflow(true);
			System.out.println("Well thats a big one :O");
			//machina.getMemory().printMemory();
		} else {
			machina.getC().setFlagOverflow(false);
		}
	}
	
	/**
	 * DIxy - Division
	 * R1 = R1 div [x*10 + y]; R2 = R1 mod [x*10 + y];
	 * @param command
	 * @throws NumberFormatException
	 */
	private void cmDIxy(String command) throws NumberFormatException{
		int address;
		try{
			address = Integer.parseInt(getValue(command, 2));
		} catch (NumberFormatException e){
			e.printStackTrace();
			throw(e);
		}
		
		int valR1 = machina.getR1().getValInt();
		int valXY = machina.getMemory().
				getWordAtAddress(address).getValInt();
		
		int divVal = valR1 / valXY;
		int modVal = valR1 % valXY;
		
		machina.getR1().setValInt(divVal);
		machina.getR2().setValInt(modVal);
	}
	
	/**
	 * DEC[x], DEC[x], INC[x], INC[x]
	 * Increments/Decrements R[x] register
	 * @param command
	 * @throws IllegalArgumentException
	 */
	private void cmDecInc(String command) throws IllegalArgumentException{
		FRegister4B reg = null;
		
		switch (command.charAt(3)){
		case '1':
			reg = machina.getR1();
			break;
		case '2':
			reg = machina.getR2();
			break;
		default:
			throw new IllegalArgumentException(command);
		}
		
		switch (command.charAt(0)){
		case 'D':
			reg.setValInt(reg.getValInt() - 1); 
			break;
		case 'I':
			reg.setValInt(reg.getValInt() + 1); 
			break;
		default:
			throw new IllegalArgumentException(command);
		}
	}
	
	/**
	 * Czxy - Compare
	 * Compares [R[z]] with value at [x*10 + y]
	 * @param command
	 * @throws IllegalArgumentException
	 */
	private void cmCzxy(String command) throws IllegalArgumentException{
		FRegister4B reg = null;
		int address = Integer.parseInt(getValue(command, 2));
		int valXY = machina.getMemory().
				getWordAtAddress(address).getValInt();
		
		switch (command.charAt(1)){
		case '1':
			reg = machina.getR1();
			break;
		case '2':
			reg = machina.getR2();
			break;
		default:
			System.out.println(command.charAt(2));
			throw new IllegalArgumentException(command);
		}
		
		int regVal = reg.getValInt();
		FRegisterLogicByte lReg = machina.getC();
		if (regVal > valXY){
			lReg.setFlagZero(false);
			lReg.setFlagSign(false);
			lReg.setFlagOverflow(false);
		} else if (regVal < valXY){
			lReg.setFlagZero(false);
			lReg.setFlagSign(true);
			lReg.setFlagOverflow(false);
		} else if (regVal == valXY){
			lReg.setFlagZero(true);
			lReg.setFlagSign(false);
			lReg.setFlagOverflow(false);
		}
	}
	
	/**
	 * JUMPS!
	 * JPxy (jump), 
	 * JExy (jump if equal), JLxy (jump if lower), JGxy (jump if greater) 
	 * @param command
	 * @throws IllegalArgumentException
	 */
	private void cmJUMPxy(String command) throws IllegalArgumentException{
		int address = Integer.parseInt(getValue(command, 2));
		
		switch(command.charAt(1)){
		//Fluffy JUMPS!!! 
		case 'P': //JP
			machina.getIC().setValInt(address);
			break;
		case 'E': //JE
			if (machina.getC().isFlagZero())
				machina.getIC().setValInt(address);
			break;
		case 'L': //JL
			if (!machina.getC().isFlagZero() && 
					machina.getC().isFlagOverflow() == 
						machina.getC().isFlagSign())
				machina.getIC().setValInt(address);
			break;
		case 'G': //JG
			if (!machina.getC().isFlagZero() && 
					machina.getC().isFlagOverflow() != 
						machina.getC().isFlagSign())
				machina.getIC().setValInt(address);
			break;
		default:
			throw new IllegalArgumentException(command);
		}
	}
	
	/**
	 * INxy - Input
	 * Calls interrupt for input to [xy]
	 * @param command
	 */
	private void cmINxy(String command) {
		machina.getR2().setValInt(
				Integer.parseInt(getValue(command, 2)));
		machina.getFMachine().getSI().setValue(1);
	}
	
	/**
	 * OPxy = Output
	 * Calls interrupt for output from [xy]
	 * @param command
	 */
	private void cmOPxy(String command) {
		machina.getR2().setValInt(
				Integer.parseInt(getValue(command, 2)));
		machina.getFMachine().getSI().setValue(2);
	}
	
	/**
	 * HALT
	 * Calls interrupt to stop VM
	 */
	private void cmHALT() {
		machina.getFMachine().getSI().setValue(3);
		System.out.println("At last! The fluffyness " +
				"has come to an end!!! MUAHAHAHAHAHALT!!!");
	}
	
	/**
	 * Get IC
	 * Puts IC value to R2
	 */
	private void cmGEIC() {
		int val = machina.getIC().getValInt();
		machina.getR2().setValInt(val);
	}
	
	/**
	 * Get C[x]
	 * Puts C[x] value to R2
	 * x = 1 - Zero Flag
	 * x = 2 - Sign Flag
	 * x = 3 - Overflow Flag
	 * @param command
	 * @throws IllegalArgumentException
	 */
	private void cmGECx(String command) throws IllegalArgumentException{
		int val = 0;
		char lastChar = command.charAt(3);
		
		switch (lastChar){
		case '1':
			if (machina.getC().isFlagZero()){
				val = 1;
			} else{
				val = 0;
			}
			break;
		case '2':
			if (machina.getC().isFlagSign()){
				val = 1;
			} else{
				val = 0;
			}
			break;
		case '3':
			if (machina.getC().isFlagOverflow()){
				val = 1;
			} else{
				val = 0;
			}
			break;
		default:
			throw new IllegalArgumentException(command);
		}
		
		machina.getR2().setValInt(val);
	}
	
	/**
	 * Get SP
	 * puts SP value to R2
	 */
	private void cmGESP() {
		int val = machina.getSP().getValInt();
		machina.getR2().setValInt(val);
	}
	
	/**
	 * Set IC
	 * Sets IC value to R2 value
	 */
	private void cmSEIC() {
		int val = machina.getR2().getValInt();
		machina.getIC().setValInt(val);
	}
	
	/**
	 * Set SP
	 * Sets SP value to R2 value
	 */
	private void cmSESP() {
		int val = machina.getR2().getValInt();
		machina.getIC().setValInt(val);
	}
	
	/**
	 * Set C[x]
	 * Sets C[x] value to R2 value
	 * if R2 value is NOT "0" or "1" -> FALSE
	 * x = 1 - Zero Flag
	 * x = 2 - Sign Flag
	 * x = 3 - Overflow Flag
	 * @throws IllegalArgumentException
	 */
	private void cmSECx(String command) throws IllegalArgumentException {
		boolean val = false;
		if (machina.getR2().getValInt() == 1){
			val = true;
		}
		
		char lastChar = command.charAt(3);
		
		switch (lastChar){
		case '1':
			machina.getC().setFlagZero(val);
			break;
		case '2':
			machina.getC().setFlagSign(val);
			break;
		case '3':
			machina.getC().setFlagOverflow(val);
			break;
		default:
			throw new IllegalArgumentException(command);
		}	
	}
	
	
}
