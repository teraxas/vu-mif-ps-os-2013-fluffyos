
package fluffy.machine.virtual;

import java.io.File;
import java.io.IOException;

import fluffy.machine.FMachine;
import fluffy.machine.registers.FRegister2B;
import fluffy.machine.registers.FRegister4B;
import fluffy.machine.registers.FRegisterLogicByte;

/**
 * Virtual machine
 * @author karolis
 */
public class VMachine {

	/**
	 * The real machine
	 * TODO take only registers - not the whole machine object
	 */
	private FMachine fMachine; 
	
	/**
	 * Program name
	 */
	private String name;
	
	/**
	 * this is where all the fluffy stuff happens ^-^
	 */
	private VMCommandProcessor proc;
	
	/**
	 * Memory
	 */
	private VMemory memory;
	
	/**
	 * Data register
	 */
	private FRegister4B regR1, regR2;
	
	/**
	 * Code index register
	 */
	private FRegister2B regIC;
	
	/**
	 * 1 byte logic register
	 */
	private FRegisterLogicByte regC;
	
	/**
	 * Stack pointer register
	 */
	private FRegister4B regSP;
	
	/**
	 * The IOI register value machine is waiting for.
	 * "0" if not waiting.
	 * 1 - input
	 * 2 - output
	 */
	private int waitingIOI = 0;
	
	/**
	 * Default constructor
	 * @param fMachine 
	 * @param vmemory allocated virtual memory
	 */
	public VMachine(FMachine fMachine, VMemory vmemory) {
		this.initDefaultRegisters();
		this.memory = vmemory;
		this.proc = new VMCommandProcessor(this);
		this.fMachine = fMachine;
	}

	/**
	 * Initializes registers with default values
	 */
	private void initDefaultRegisters() {
		this.regC = new FRegisterLogicByte();
		this.regIC = new FRegister2B();
		this.regR1 = new FRegister4B();
		this.regR2 = new FRegister4B();
		this.regSP = new FRegister4B();
	}
	
	/**
	 * Loads program to memory from file
	 * @param filepath filepath to source file
	 * @throws IOException 
	 */
	public void loadProgramFromFile(String filepath) throws IOException {
		File source = new File(filepath);
		proc.loadMemory(source);
			
	}
	
	/**
	 * VM step
	 */
	public void step() {
		proc.processCommand(this.getNextCommand());
	}
	
	/**
	 * Gets next command from memory
	 * @return
	 */
	private String getNextCommand(){
		int codeIndex = regIC.getValInt();
		String command = this.getMemory().getWordAtAddress(
				codeIndex).getVal();
		if (regIC.getValInt() == codeIndex){
			codeIndex++;
			regIC.setValInt(codeIndex);
		}
		return command;
	}
	
	/**
	 * Sets all virtual memory blocks to unused
	 */
	public void destroy() {
		for (int i = 0; i < memory.DEFAULT_MEMSIZE; i++){
			memory.returnMem();
		}
	}
	
	/**
	 * @return the memory
	 */
	public VMemory getMemory() {
		return memory;
	}

	/**
	 * @return the r1
	 */
	public FRegister4B getR1() {
		return regR1;
	}

	/**
	 * @return the r2
	 */
	public FRegister4B getR2() {
		return regR2;
	}

	/**
	 * @return the iC
	 */
	public FRegister2B getIC() {
		return regIC;
	}

	/**
	 * @return the c
	 */
	public FRegisterLogicByte getC() {
		return regC;
	}

	/**
	 * @return the sP
	 */
	public FRegister4B getSP() {
		return regSP;
	}

	/**
	 * @param memory the memory to set
	 */
	public void setMemory(VMemory memory) {
		this.memory = memory;
	}

	/**
	 * @return the proc
	 */
	public VMCommandProcessor getProc() {
		return proc;
	}

	public FMachine getFMachine(){
		return this.fMachine;
	}

	/**
	 * @return the waitingIOI
	 */
	public int getWaitingIOI() {
		return waitingIOI;
	}

	/**
	 * @param waitingIOI the waitingIOI to set
	 */
	public void setWaitingIOI(int waitingIOI) {
		this.waitingIOI = waitingIOI;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
