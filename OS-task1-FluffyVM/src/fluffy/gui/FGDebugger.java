package fluffy.gui;

import javax.swing.JButton;
import javax.swing.JFrame;

import fluffy.machine.FMachine;

/**
 * Debugger window for step mode
 * Allows to review memory and registers
 * @author karolis
 */
public class FGDebugger extends JFrame{

	private FMachine fMach;
	
	private JButton butStep;
	
	//TODO Registers table
	//TODO Memory table 
	
	public FGDebugger() {
		// TODO Auto-generated constructor stub
	}

	private void updateRegisters() {
		// TODO update registers table

	}
	
	private void updateMemory() {
		// TODO update memory table

	}
	
	private void initButs() {
		// TODO debug buttons

	}
	
	private void initGUI() {
		// TODO debug gui

	}
	
}
