package fluffy.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fluffy.machine.registers.FRegisterInterrupt;

/**
 * IO device controlled by IO channels processor
 */
public class FGDeviceIOI extends JFrame{
	
	private static final long serialVersionUID = -9107672159831964482L;

	private JTextField textFieldInput;
	private JButton buttonEnter;
	private JTextArea textOutput;
	private JLabel statusLabel;
	
	private String bufferInput;
	
	private FRegisterInterrupt regIOI;
	
	public FGDeviceIOI(FRegisterInterrupt regIOI) {
		initGUI();
		initButs();
		this.regIOI = regIOI;
		//frameStatus();//DEBUG
	}
	
	/**
	 * Put line on output
	 * @param line
	 */
	public void makeOutput(String line) {
		String str = line + '\n';
		textOutput.append(str);
	}
	
	/**
	 * Get line from input
	 * @return
	 */
	public void makeInput() {
		System.out.println("   field: " + this.textFieldInput.isEditable());
		System.out.println("   button: " + this.buttonEnter.isEnabled());
		this.textFieldInput.setEditable(true);
		this.buttonEnter.setEnabled(true);
		//frameStatus();//DEBUG
		System.out.println("Input enabled");
		statusLabel.setText("Waiting for input");
	}
	
	/**
	 * Initialize buttons
	 */
	private void initButs() {
		//Input button
		this.buttonEnter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				enterLine();
			}
		});
		
		this.textFieldInput.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				enterLine();
			}
		});
	}
	
	private void enterLine() {
		this.setBuffer(textFieldInput.getText());
		this.textFieldInput.setText("");
		this.textFieldInput.setEditable(false);
		this.buttonEnter.setEnabled(false);
		this.regIOI.setValue(1);
		System.out.println("Input complete");
		statusLabel.setText("Ready");
	}
	
	private void initGUI(){
		setTitle("Fluffy IO Device");
        setSize(400, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);//Ar exit?...

        JPanel panel = new JPanel();

        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel.setLayout(new GridLayout(0, 1, 0, 5));
     
        textOutput = new JTextArea();
        textOutput.setLineWrap(true);
        textOutput.setWrapStyleWord(true);
        textOutput.setEditable(false);
        
        JScrollPane scrollOutput = new JScrollPane(textOutput);
        
        JLabel labelInput = new JLabel("Įvedimas");
        textFieldInput = new JTextField();
        textFieldInput.setEditable(false);
        
        buttonEnter = new JButton("Įvesti");
        buttonEnter.setEnabled(false);
        
        this.statusLabel = new JLabel("Ready");
        
        panel.add(scrollOutput);
        panel.add(labelInput);
        panel.add(textFieldInput);
        panel.add(buttonEnter);
        panel.add(statusLabel);
        
        add(panel);
        setVisible(true);
	}

	/**
	 * @return the buffer
	 */
	public String getBuffer() {
		return bufferInput;
	}

	/**
	 * @param buffer the buffer to set
	 */
	public void setBuffer(String buffer) {
		this.bufferInput = buffer;
	}
	
	private void frameStatus() {
		System.out.println("   field: " + this.textFieldInput.isEditable());
		System.out.println("   button: " + this.buttonEnter.isEnabled());
		System.out.println("   visibility: " + this.isVisible());
		System.out.println("   active: " + this.isActive());
		System.out.println("   enabled: " + this.isEnabled());
		System.out.println("   focusable: " + this.isFocusable());
		System.out.println("   showing: " + this.isShowing());
		System.out.println("   valid:" + this.isValid());
	}
	
}
