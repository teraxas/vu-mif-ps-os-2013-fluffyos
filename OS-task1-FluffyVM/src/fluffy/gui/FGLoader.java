/**
 * 
 */
package fluffy.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fluffy.machine.FMachine;

/**
 * @author karolis
 *
 */
public class FGLoader extends JFrame{

	private static final long serialVersionUID = -1683875339290164654L;
	
	/**
	 * The machine
	 */
	private FMachine fMach;
	
	JLabel fNameLabel;
	JTextField fNameField;
	
	JButton loadButton;
	JButton runButton;
	JButton stepModeButton;
	
	JLabel statusLabel;
	
	
	/**
	 * Constructor
	 * @param fMachine
	 */
	public FGLoader(FMachine fMachine) {
		this.fMach = fMachine;
		this.initGUI();
		this.initButs();
	}
	
	/**
	 * Initialize buttons
	 */
	private void initButs() {
		this.loadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadProgram();
			}
		});
		
		this.fNameField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadProgram();
			}
		});
		
		this.runButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runProgram();
			}
		});
		
		this.stepModeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO step button
				
			}
		});
	}
	
	/**
	 * Creates VM and loads program to memory
	 */
	private void loadProgram() {
		String filepath = fNameField.getText();
		filepath = "fluffies/" + filepath + ".fluffy";
		
		try {
			this.fMach.createTheVM(filepath);
		} catch (IOException e) {
			// TODO Wrong filename
			System.out.println("Wrong filename");
			statusLabel.setText("Wrong filename");
			e.printStackTrace();
			return;
		}
		
		statusLabel.setText("Program loaded");
		this.runButton.setEnabled(true);
		this.stepModeButton.setEnabled(true);
		this.loadButton.setEnabled(false);
		this.fNameField.setEditable(false);
		
		this.runButton.requestFocusInWindow();
	}
	
	/**
	 * Starts main loop
	 */
	private void runProgram() {
		this.setVisible(false);
		this.fMach.mainLoopSingleJob();
	}
	
	/**
	 * Initialize GUI for single job loading
	 */
	private void initGUI(){
		setTitle("Fluffy OS loader");
        setSize(400, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel panel = new JPanel();

        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel.setLayout(new GridLayout(0, 1, 0, 5));

        this.fNameLabel = new JLabel("Filename:");
        this.fNameField = new JTextField();
        this.loadButton = new JButton("Load file");
        
        this.runButton = new JButton("Run");
        this.runButton.setEnabled(false);
        
        this.stepModeButton = new JButton("Step mode");
        stepModeButton.setEnabled(false);
        
        this.statusLabel = new JLabel("Ready");
        
        panel.add(fNameLabel);
        panel.add(fNameField);
        panel.add(loadButton);
        panel.add(runButton);
        panel.add(stepModeButton);
        panel.add(statusLabel);
        
        add(panel);
        setVisible(true);
	}
}
