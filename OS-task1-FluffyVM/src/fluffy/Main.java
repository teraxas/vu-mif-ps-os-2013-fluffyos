/**
 * 
 */
package fluffy;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fluffy.gui.FGLoader;
import fluffy.machine.FMachine;

/**
 * @author karolis
 *
 */
public class Main {

	/**
	 * Is test
	 */
	static final boolean TEST = false;
	
	/**
     * Sets system look and feel
     */
    static void initLookAndFeel() {
            try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
            } catch (InstantiationException e1) {
                    e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                    e1.printStackTrace();
            }
    }
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Fluffy Operating System");
		initLookAndFeel();
		
		if (!TEST){
			//Create a Fluffy machine
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					FMachine fluffyMachine = new FMachine();
					@SuppressWarnings("unused")
					FGLoader fGUI = new FGLoader(fluffyMachine);
				}
			});
		} else {
			//Let's test!
			System.out.println("Fluffy TEST! ^.^");
			FluffynessTest testing = new FluffynessTest();
		}    
		
	}

}
