package fluffy;

import java.io.IOException;

import fluffy.machine.FMachine;

/**
 * Testing class
 * @author karolis
 */
public class FluffynessTest {

	/**
	 * Test filepath
	 */
	static public final String TEST_FILEPATH = 
			"fluffies/t_input.fluffy";
	
	/**
	 * Default constructor
	 */
	public FluffynessTest(){
		this(TEST_FILEPATH);
	}
	
	/**
	 * Constructor
	 */
	public FluffynessTest(String fp) {
		//Tests to do:
		runTest(fp);
	}
	
	/**
	 * Test for running machine
	 */
	private void runTest(String fp){
		FMachine mach = new FMachine();
		try {
			mach.createTheVM(fp);
			//mach.getMemory().printMemory();
		} catch (IOException e) {
			System.out.println("Bad filename in runTest");
			return;
		}
		mach.mainLoopSingleJob();
	}
	
}
