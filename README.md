*Fluffy operating system for FluffyVM*

**The Fluffyest operating system in the world!!! ^.^**

* VM project is available in `OS-task1-FluffyVM` folder.

* Documentation is available in `docs` folder.

* VM program files (`*.fluffy`) are available in `fluffies` folder.

To compile and run Fluffy OS on Linux simply type "make" and "make run" in terminal. 

Both Eclipse and Netbeans project files available.
Netbeans were used as GUI editor.

*Git repository is available at BitBucket: https://bitbucket.org/teraxas/vu-mif-ps-os-2013-fluffyos*


